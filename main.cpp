#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SIZE 101

int COLISOES = 0;

int hashcode(char chave[15]){
    int hash=0;

    for(int i=0;i<strlen(chave);i++) hash+=19*chave[i]*(i+1);

    return hash;
}

typedef struct no{
    char valor[15];
    struct no *prox;
}*itemE;

char addE(itemE table[SIZE],char chave[15]){
    itemE x = (itemE) malloc(sizeof(struct no));
    strcpy(x->valor,chave);
    x->prox=NULL;

    int col = 0;
    int i = hashcode(chave)%SIZE;
    if(table[i]==NULL)
        table[i]=x;
    else{
        itemE aux = table[i];
        while(aux->prox!=NULL){
            if(strcmp(aux->valor, chave)) return 0;

            col++;
            aux = aux->prox;
        }
        aux->prox = x;
    }
    
    COLISOES += col;
    return 1;
}

int findE(itemE table[SIZE],char chave[15], itemE* item){
    int i=hashcode(chave)%SIZE;
    
    itemE aux=table[i];
    while(aux!=NULL){
        if(strcmp(chave,aux->valor)==0){
            *item = aux;
            return i;
        }
        
        aux=aux->prox;
    }
    return -1;
}

char remE(itemE table[SIZE],char chave[15]){
    int i=hashcode(chave)%SIZE;
    
    itemE aux=table[i];
    if(aux == NULL) return 0;
    if(strcmp(chave,aux->valor)==0){
        table[i] = aux->prox;
        free(aux);
        return i;
    }

    while(aux->prox!=NULL){
        if(strcmp(chave,aux->prox->valor)==0){
            itemE del = aux->prox;
            aux->prox = del->prox;

            free(del);
            return i;
        }
        
        aux=aux->prox;
    }
    return 0;
}

typedef struct{
    char valor[15];
    int tentativas;
}itemA;

char addA(itemA table[SIZE],char chave[15]){
    int i=hashcode(chave)%SIZE;
    
    for(int j=0;j<20;j++){
        int k=(i+j*j+23*j)%SIZE;
        if(strcmp(chave, table[k].valor)==0) return 2;

        if(strcmp(table[k].valor, "")==0){
            table[k].tentativas = j;
            strcpy(table[k].valor, chave);

            COLISOES += j;
            return 1;
        }
    }

    return 0;
}

int findA(itemA t[SIZE], char chave[15]){
    int i=hashcode(chave)%SIZE;
    
    for(int j=0;j<20;j++){
        int k = (i+j*j+23*j)%SIZE;

        if(strcmp(t[k].valor, "")==0) return -1;

        if(strcmp(t[k].valor, chave)==0)
            return k;
    }
    return -1;
}

char remA(itemA t[101],char chave[15]){
    int i=findA(t,chave);
    if(i == -1) return 0;
        
    strcpy(t[i].valor,"");
    return 1;  
}

void initHashA(itemA tabela[SIZE]){
    for(int i=0;i<SIZE;i++)
        strcpy(tabela[i].valor,"");
}

void initHashE(itemE tabela[SIZE]){
    for(int i=0;i<101;i++)
        tabela[i] = NULL;
}

int fprintA(FILE *stream, itemA tabela[SIZE]){
    int count = 0;

    for(int i=0; i<SIZE; i++){
        if(strcmp(tabela[i].valor, "")!=0){
            fprintf(stream, "    [%d] chave:%s, tentativas:%d\n", i, tabela[i].valor, tabela[i].tentativas);
            count++;
        }
    }

    return count;
}

void fprintItemE(FILE *stream, itemE tabela){
    itemE aux = tabela;

    while(aux->prox!=NULL){
        fprintf(stream, "%s -> ", aux->valor);
        aux = aux->prox;
    }

    fprintf(stream, "%s\n", aux->valor);
}

void fprintE(FILE *stream, itemE tabela[SIZE]){
    for(int i=0; i<SIZE; i++){
        if(tabela[i] != NULL){
            fprintf(stream, "    [%d]: ", i);
            fprintItemE(stream, tabela[i]);
        }
    }
}

int main(){
    itemA tabelaA[SIZE];
    itemE tabelaE[SIZE];
    
    initHashA(tabelaA);
    initHashE(tabelaE);

    FILE *ent = fopen("entrada.txt","r");
    FILE *rel = fopen("relatorio.txt","w");

    fprintf(rel, "Relatorio do Programa:\nOperacoes:\n");
    char op;
    char chave[15];
    while(fscanf(ent, " %c %s", &op, chave)!=EOF){
        switch (op){
        case 'a':
            addA(tabelaA, chave);
            addE(tabelaE, chave);

            fprintf(rel, "    add %s\n", chave);
            break;
        case 'r':
            remA(tabelaA, chave);
            remE(tabelaE, chave);

            fprintf(rel, "    rem %s\n", chave);
            break;
        default:
            fprintf(rel, "ERROR! NOT VALID OPERATION\n");
            printf("ERROR! NOT VALID OPERATION\n");
            break;
        }
    }
    fclose(ent);

    fprintf(rel, "\nTotal de colisoes: %d\n", COLISOES);
    fprintf(rel, "\nTabela Enderecamento Aberto:\n");
    int nChaves = fprintA(rel, tabelaA);

    fprintf(rel, "\nTotal de chaves armazenadas:%d\n", nChaves);
    fprintf(rel, "Tabela Encadeamento Externo:\n");
    fprintE(rel, tabelaE);

    fclose(rel);
}