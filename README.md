# Hashtable - EDI 

Quarto trabalho de Estrutura de Dados I, da UNESP - Bauru BCC22.

## Objetivo do trabalho

Construir uma Hashtable com enderecamento aberto como estrategia de tratamento de colisao e outra hashtable com encadeamento externo como estrategia de tratamento de colisao e seus metodos:
- ADD: insere uma nova chave na tabela (ignore a inserção da chave que já
existe),
- FIND: encontra o índice do elemento definido pela chave (ignore, se tal
elemento não existir),
- REM: excluir uma chave da tabela (sem mover as outras), marcando a posição
na tabela como vazia (ignorar chaves inexistentes na tabela)

## Entrada
O arquivo arquivo entrada.txt representa as operacoes feitas a ambas as Hashtables sendo sua sintaxe a seguinte:

``` op chave ```

Sendo op um char referente a uma das seguintes operacoes: 
- a: Adicionar
- r: remover

E chave, a chave a ser inserida.

Exemplo:
```
a Pedro
a Gerso
a Roberso
r Gerso
```

## Obrigado

![Pela lendo](https://upload-os-bbs.hoyolab.com/upload/2022/09/02/102177407/3cf56da0de59ffd6a6a3706e089e5bcd_8488090151786451384.gif)
